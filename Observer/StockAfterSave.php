<?php

namespace Eparts\ProdutoVariavel\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\ObjectManagerInterface;
use Psr\Log\LoggerInterface;
use Magento\Catalog\Model\ProductFactory;
use Eparts\ProdutoVariavel\Helper\Data;


class StockAfterSave implements ObserverInterface
{

    protected $productRepository;
    protected $objManager;
    protected $logger;
    protected $stock;
    protected $productFactory;
    protected $helper;
    protected $dateTime;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        ObjectManagerInterface $objectManager,
        LoggerInterface $logger,
        ProductFactory $productFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        Data $data
    )
    {
        $this->objManager = $objectManager;
        $this->productRepository = $productRepository;
        $this->logger = $logger;
        $this->productFactory = $productFactory;
        $this->helper = $data;
        $this->dateTime = $dateTime;
    }

    /**
     * @param EventObserver $observer
     * @return false
     */
    public function execute(EventObserver $observer): bool
    {

        try {
            $order = $observer->getEvent()->getOrder();
            $status  = $order->getStatus();
            $createdAt =   $this->dateTime->gmtTimestamp($order->getCreatedAt());
            $updateAt =   $this->dateTime->gmtTimestamp($order->getUpdateAt());

            if($createdAt == $updateAt){
                $skusUpdated = [];
                foreach ($order->getAllItems() as $item) {

                    if ($item->getProductType() == 'bundle') {
                        continue;
                    }

                    $qtyOrdered = intval($item->getQtyOrdered());
                    $product = $this->productRepository->get($item->getSku());
                    $ids = $this->loadAllSkus($product->getData($this->helper->getAttributeCustomSku()), $product->getId());

                    if (empty($ids)) {
                        return false;
                    }

                    if (!empty($skusUpdated) && in_array($product->getData($this->helper->getAttributeCustomSku()), $skusUpdated)) {
                        continue;
                    }

                    $qtyActual = $this->helper->getStockQty($product->getSku());

                    $qty = $qtyActual - $qtyOrdered;
                    foreach ($ids as $id) {
                        $isInStock = 1;

                        if (empty($qty)) {
                            $isInStock = 0;
                        }

                        $data = [
                            'qty' => $qty,
                            'is_in_stock' => $isInStock
                        ];

                        if ($id == $product->getId()) {
                            continue;
                        }

                        $this->helper->updateProductStock($id, $data);

                        $skusUpdated[] = $product->getData($this->helper->getAttributeCustomSku());
                    }
                }
            }
        } catch (\Exception $e) {
            $this->logger->error('Erro ao dar baixa no estoque: ' . $e->getMessage());
        }

        return true;
    }

    /**
     * load all product with $skuErp equals
     *
     * @param $skuErp
     * @param $id
     * @return array
     */
    protected function loadAllSkus($skuErp, $id): array
    {
        $collection = $this->productFactory->create()->getCollection()->addAttributeToFilter($this->helper->getAttributeCustomSku(), $skuErp);
        $ids = [];
        foreach ($collection as $object) {
            $ids[] = $object->getEntityId();
        }

        return $ids;

    }
}