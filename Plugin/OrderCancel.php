<?php

namespace Eparts\ProdutoVariavel\Plugin;

use Eparts\ProdutoVariavel\Helper\Data;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\ObjectManagerInterface;
use Magento\Sales\Model\Order;

class OrderCancel
{

    protected $productRepository;
    protected $objManager;
    protected $stock;
    protected $productFactory;
    protected $helper;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        ObjectManagerInterface $objectManager,
        ProductFactory $productFactory,
        Data $data
    )
    {
        $this->objManager = $objectManager;
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
        $this->helper = $data;
    }

    public function afterCancel(Order $order, $result)
    {
        foreach ($order->getAllVisibleItems() as $item) {
            $qtyOrdered = intval($item->getQtyOrdered());
            $product = $this->productRepository->get($item->getSku());
            $ids = $this->loadAllSkus($product->getData($this->helper->getAttributeCustomSku()), $product->getId());

            if (empty($ids)) {
                return $result;
            }

            if (!empty($skusUpdated) && in_array($product->getData($this->helper->getAttributeCustomSku()), $skusUpdated)) {
                continue;
            }

            $qtyActual = $this->helper->getStockQty($product->getSku()) - $qtyOrdered;
            $qty = $qtyActual + $qtyOrdered;
            foreach ($ids as $id) {
                $data = [
                    'qty' => $qty,
                    'is_in_stock' => 1
                ];

                if ($id == $product->getId()) {
                    continue;
                }

                $this->helper->updateProductStock($id, $data);
                $skusUpdated[] = $product->getData($this->helper->getAttributeCustomSku());
            }
        }

        return $result;
    }

    protected function loadAllSkus($skuErp, $id): array
    {
        $collection = $this->productFactory->create()->getCollection()->addAttributeToFilter($this->helper->getAttributeCustomSku(), $skuErp);
        $ids = [];
        foreach ($collection as $object) {
            $ids[] = $object->getEntityId();
        }

        return $ids;

    }
}