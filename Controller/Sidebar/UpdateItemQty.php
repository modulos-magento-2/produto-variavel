<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Eparts\ProdutoVariavel\Controller\Sidebar;

use Magento\Checkout\Controller\Sidebar\UpdateItemQty as UpdateItemQtyParent;
use Magento\Checkout\Model\Sidebar;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Response\Http;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Json\Helper\Data;
use Psr\Log\LoggerInterface;
use Eparts\ProdutoVariavel\Helper\Data as HelperProdutoVariavel;
use Magento\Checkout\Model\Session as CheckoutSession;

class UpdateItemQty extends UpdateItemQtyParent
{
    /**
     * @var Sidebar
     */
    protected $sidebar;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Data
     */
    protected $jsonHelper;

    protected $helperProdutoVariavel;

    protected $session;

    /**
     * @param Context $context
     * @param Sidebar $sidebar
     * @param LoggerInterface $logger
     * @param Data $jsonHelper
     * @param HelperProdutoVariavel $helperProdutoVariavel
     * @codeCoverageIgnore
     */
    public function __construct(
        Context $context,
        Sidebar $sidebar,
        LoggerInterface $logger,
        Data $jsonHelper,
        HelperProdutoVariavel $helperProdutoVariavel,
        CheckoutSession $session
    )
    {
        $this->sidebar = $sidebar;
        $this->logger = $logger;
        $this->jsonHelper = $jsonHelper;
        $this->helperProdutoVariavel = $helperProdutoVariavel;
        $this->session = $session;
        parent::__construct($context, $sidebar, $logger, $jsonHelper);
    }


    /**
     * @return UpdateItemQty|Http
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        $itemId = (int)$this->getRequest()->getParam('item_id');
        $itemQty = $this->getRequest()->getParam('item_qty') * 1;
        $item = $item = $this->session->getQuote()->getItemById($itemId);

        $id = $item->getProductId();

        if ($item->getProduct()->getTypeId() == 'configurable') {
            $id = key($item->getQtyOptions());
        }

        #Valida Estoque Produtos Variáveis
        $isValidStock = $this->helperProdutoVariavel->validateStock($this->session->getQuote(), ['product' => $id, 'qty' => $itemQty]);
        if (!$isValidStock) {
             return $this->jsonResponse('Quantidade não disponível em estoque');
        }

        try {
            $this->sidebar->checkQuoteItem($itemId);
            $this->sidebar->updateQuoteItem($itemId, $itemQty);
            return $this->jsonResponse();
        } catch (LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Compile JSON response
     *
     * @param string $error
     * @return Http
     */
    protected function jsonResponse($error = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($this->sidebar->getResponseData($error))
        );
    }
}
