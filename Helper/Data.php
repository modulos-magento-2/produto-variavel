<?php

namespace Eparts\ProdutoVariavel\Helper;

use Magento\Catalog\Model\Product;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\CatalogInventory\Api\StockStateInterface;

use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\InventorySalesAdminUi\Model\GetSalableQuantityDataBySku;


class Data extends AbstractHelper
{
    protected $_product;
    protected $_stockStateInterface;
    protected $_stockRegistry;

    protected $_productRepository;
    protected $_salableQuantity;

    protected $_moduleManager;
    /**
     * @param Context $context
     * @param Product $product
     * @param StockStateInterface $stockStateInterface
     * @param StockRegistryInterface $stockRegistry
     */
    public function __construct(
        Context $context,
        Product $product,
        StockStateInterface $stockStateInterface,
        StockRegistryInterface $stockRegistry,
        ProductRepository $_productRepository,
        \Magento\Framework\Module\Manager $moduleManager
        
    )
    {
        $this->_product = $product;

        $this->_stockStateInterface = $stockStateInterface;
        $this->_stockRegistry = $stockRegistry;
        $this->_productRepository = $_productRepository;
        // $this->_salableQuantity = $getSalableQuantityDataBySku;
        $this->_moduleManager = $moduleManager;
        parent::__construct($context);
    }

    /**
     * @param $productId
     * @param $stockData
     * @throws \Exception
     */
    public function updateProductStock($productId, $stockData)
    {
        $stockItem = $this->_stockRegistry->getStockItem($productId);
        $stockItem->setData('is_in_stock', $stockData['is_in_stock']);
        $stockItem->setData('qty', (string)$stockData['qty']);
        $stockItem->save();
    }

    /**
     * @param $sku
     * @return int
     */
    public function getStockQty($sku): int
    {
         /** 
         * Verifica se o modulo de reserva de estoque esta ativoa 
         */
        if ($this->_moduleManager->isEnabled('Magento_InventorySalesAdminUi')) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $getSalableQuantityDataBySku = $objectManager->get('Magento\InventorySalesAdminUi\Model\GetSalableQuantityDataBySku');
            return intval($getSalableQuantityDataBySku->execute($sku)[0]['qty']);
        }else{
            $stockItem = $this->_stockRegistry->getStockItemBySku($sku);
            $qty = $stockItem->getQty();
            return $qty;
        }

    }

    /**
     * @param $quote
     * @param $requestInfo
     * @param bool $sumItems
     * @return bool
     */
    public function validateStock($quote, $requestInfo, bool $sumItems = true): bool
    {
        try {
            $id = $requestInfo['product'];
            if (isset($requestInfo['selected_configurable_option']) && !empty($requestInfo['selected_configurable_option'])) {
                $id = $requestInfo['selected_configurable_option'] ?? $id;
            }

            $_productLoad = $this->_productRepository->getById($id);
            $skuErp = $_productLoad->getData($this->getAttributeCustomSku());
            $qtyInQuote = 1;
            if (isset($requestInfo['qty'])) {
                $qtyInQuote = $requestInfo['qty'];
            }

            if ($_productLoad->getTypeId() == 'bundle') {

                $productsBundle = $_productLoad->getExtensionAttributes()->getBundleProductOptions();
                if (!empty($productsBundle)) {
                    $productsBundle = $productsBundle[0]->getData('product_links');
                    foreach ($productsBundle as $bundle) {
                        $qtyInQuote = intval($bundle->getData('qty') * $qtyInQuote);
                        $idBundle = $bundle->getEntityId();
                        $productChild = $this->_productRepository->getById($idBundle);
                        if ($productChild->getData($this->getAttributeCustomSku()) == $skuErp) {
                            $qtyAvailable = $this->getStockQty($productChild->getSku());
                            break;
                        }
                    }
                }
            } else {
                $qtyAvailable = $this->getStockQty($_productLoad->getSku());
            }

            if (!empty($quote)) {
                $items = $quote->getAllItems();
                if (empty($items)) {
                    return true;
                }

                foreach ($items as $item) {
                    $skuProductQuote = $item->getSku();
                    if ($item->getProductType() == 'bundle') {
                        continue;
                    }

                    $productQuote = $this->_productRepository->get($skuProductQuote);
                    if ($skuErp != $productQuote->getData($this->getAttributeCustomSku())) {
                        continue;
                    }

                    if ($item->getProductId() != $id) {
                        $qtyInQuote += intval($item->getQty());
                    }
                }

                if ($qtyInQuote > $qtyAvailable) {
                    return false;
                }
            }
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @return mixed
     */
    public function getAttributeCustomSku()
    {
        $attribute =  $this->scopeConfig->getValue('produto_variavel/general/attribute_code');
        if (empty($attribute)) {
            return 'sku_erp';
        }

        return $attribute;
    }
}